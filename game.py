# Import des classes ball et paddle 
import pygame
from paddle import Paddle
from ball import Ball
 
pygame.init()
 
# Définir les deux couleurs 
BLACK = (0,0,0)
WHITE = (255,255,255)
BLUE = (52, 102, 97)
YELLOW = (230, 172, 2) 

# Déploie une fenêtre
size = (700, 500)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("PING PONG RIL19 en double 4 joueurs")
 
# Objet raquette A à droit 
paddleA = Paddle(WHITE, 10, 100)
paddleA.rect.x = 5
paddleA.rect.y = 200
 
paddleAa = Paddle(WHITE, 10, 50)
paddleAa.rect.x = 200
paddleAa.rect.y = 200

# Objet raquette B à gauche 
paddleB = Paddle(WHITE, 10, 100)
paddleB.rect.x = 690
paddleB.rect.y = 200


paddleBb = Paddle(WHITE, 10, 50)
paddleBb.rect.x = 495
paddleBb.rect.y = 250
 
# Objet balle
ball = Ball(YELLOW,12,12)
ball.rect.x = 345
ball.rect.y = 195
 
#Contient l'ensemble des paramètres de la classe sprite
all_sprites_list = pygame.sprite.Group()
 
# Ajout de deux raquettes et une balle
all_sprites_list.add(paddleA)
all_sprites_list.add(paddleAa)
all_sprites_list.add(paddleB)
all_sprites_list.add(paddleBb)
all_sprites_list.add(ball)
 
# Condition pour sortir du jeu
running = True
 
# Contrôle de nb d'image à la seconde / Eviter les sentillements
clock = pygame.time.Clock()
 
#Initialise le score
scoreA = 0
scoreB = 0
 
# ---------------- Program game -------------------
while running:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
              running = False
        elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE: 
                     running=False

    # Déplace la raquette côté gauche avec les flèches pour le joueur A et touche W S pour le joueur B
    keys = pygame.key.get_pressed()
    if keys[pygame.K_w]:
        paddleA.moveUp(5)
    if keys[pygame.K_s]:
        paddleA.moveDown(5)
    
    if keys[pygame.K_f]:
        paddleAa.moveUp(5)
    if keys[pygame.K_c]:
        paddleAa.moveDown(5)

    if keys[pygame.K_UP]:
        paddleB.moveUp(5)
    if keys[pygame.K_DOWN]:
        paddleB.moveDown(5)  

    if keys[pygame.K_j]:
        paddleBb.moveUp(5)
    if keys[pygame.K_n]:
        paddleBb.moveDown(5)   
 

    # Update la position lors du déroulement du jeu
    all_sprites_list.update()
    
    # Vérifie la position de la balle aux extrémité de l'écran
    if ball.rect.x>=690:
        scoreA+=1
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.x<=0:
        scoreB+=1
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.y>490:
        ball.velocity[1] = -ball.velocity[1]
    if ball.rect.y<0:
        ball.velocity[1] = -ball.velocity[1]     
 
    #Detecte la collision de la balle
    if pygame.sprite.collide_mask(ball, paddleA) or pygame.sprite.collide_mask(ball, paddleAa)  or pygame.sprite.collide_mask(ball, paddleBb) or pygame.sprite.collide_mask(ball, paddleB):
      ball.bounce()
    
    screen.fill(BLUE)
    #L Dessine le filet
    pygame.draw.line(screen, WHITE, [349, 0], [349, 500], 5)
    
    all_sprites_list.draw(screen) 
 
    #Afficher le score 
    font = pygame.font.Font(None, 90)
    text = font.render(str(scoreA), 1, YELLOW)
    screen.blit(text, (250,10))
    text = font.render(str(scoreB), 1, YELLOW)
    screen.blit(text, (420,10))
 
    pygame.display.flip()
     
    clock.tick(60)
 
pygame.quit()