import pygame

BLACK = (0,0,0)
 
class Paddle(pygame.sprite.Sprite):
    #Utilisation de la class sprite de Pygame pour 
    
    def __init__(self, color, width, height):
        
        super().__init__()
        
        # Constructeur de la classe
        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(BLACK)
        
 
        # Dessine une raquette avec ces paramètres
        pygame.draw.rect(self.image, color, [0, 0, width, height])
        self.rect = self.image.get_rect()

    # Fonction move au dessus    
    def moveUp(self, pixels):
        self.rect.y -= pixels
		# Vérifie la position de la raquette toujours dans l'écran
        if self.rect.y < 0:
          self.rect.y = 0

    # Fonction move en dessous      
    def moveDown(self, pixels):
        self.rect.y += pixels
        if self.rect.y > 400:
            self.rect.y = 400
    
    